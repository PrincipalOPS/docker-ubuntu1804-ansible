FROM ubuntu:18.04

RUN DEBIAN_FRONTEND=noninteractive && apt-get update && \
  apt-get install -y apt-utils python-pip python-minimal && \
  pip install ansible

RUN apt-get update -y && apt-get upgrade -y && apt-get -y clean && rm -rf /var/lib/apt/lists/*

ENTRYPOINT [ "/usr/bin/tail" ]

# CMD [ "-f", "/dev/null" ]
