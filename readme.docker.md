# docker-ubuntu1804-ansible

Docker Commands

## Docker-Compose Build and Run

docker-compose up -d --build

## Docker-Compose Stop

docker-compose stop

## Docker-Compose Bash Shell

docker-compose exec ansible bash

## Docker Commands

docker build -t ansible .

docker run -d ansible

docker ps

docker stop [containerid]

docker exec -it [containerid] bash
